/* Sensor handles */
#define HANDLE_ACCELEROMETER	(0)
#define HANDLE_MAGNETIC_FIELD	(1)
#define HANDLE_ORIENTATION		(2)
#define HANDLE_GYROSCOPE		(3)
#define HANDLE_LIGHT			(4)
#define HANDLE_PRESSURE			(5)
#define HANDLE_TEMPERATURE		(6)
#define HANDLE_PROXIMITY		(8)
#define HANDLE_MAX			    (9)

/* Sensor handles */
#define MINDELAY_ACCELEROMETER	(1000)
#define MINDELAY_MAGNETIC_FIELD	(1000)
#define MINDELAY_ORIENTATION	(1000)
#define MINDELAY_GYROSCOPE		(1000)
#define MINDELAY_LIGHT			(0)
#define MINDELAY_PRESSURE		(0)
#define MINDELAY_TEMPERATURE	(0)
#define MINDELAY_PROXIMITY		(0)

/* Constants */
#define LSM_M_MAX_CAL_COUNT 300
#define RADIANS_TO_DEGREES (180.0/M_PI)
#define DEGREES_TO_RADIANS (M_PI/180.0)

/* Magnetometer defines */
#define LSM303DLH_M_RANGE_4_0G "4"
#define LSM303DLH_M_MODE_CONTINUOUS "0"
#define LSM303DLH_M_MODE_SLEEP "3"
#define LSM303DLH_M_RATE_15_00 "4"
#define SENSOR_UX500_G_To_uT (100.0f)
#define SENSOR_UX500_MAGNETOMETER_RANGE LSM303DLH_MAGNETOMETER_RANGE_1_9G
#define GAIN_X 430
#define GAIN_Y 430
#define GAIN_Z 385

#if (SENSOR_UX500_MAGNETOMETER_RANGE == LSM303DLH_MAGNETOMETER_RANGE_1_9G)
#define SENSOR_UX500_MAGNETOMETER_MAX (1.9f * SENSOR_UX500_G_To_uT)
#define SENSOR_UX500_MAGNETOMETER_STEP (1.9f * SENSOR_UX500_G_To_uT / 2048.0f)
#else
#error Unknown range
#endif

/* Accelerometer defines */
#define LSM303DLH_A_RANGE_2G "0"
#define LSM303DLH_A_MODE_OFF "0"
#define LSM303DLH_A_MODE_NORMAL "1"
#define LSM303DLH_A_RATE_50 "0"
#define SENSOR_UX500_ACCELEROMETER_RANGE LSM303DLH_ACCELEROMETER_RANGE_4G

#define LSM303DLHC_A_MODE_NORMAL "4"
#define LSM303DLHC_A_RANGE_2G "0"

#if (SENSOR_UX500_ACCELEROMETER_RANGE == LSM303DLH_ACCELEROMETER_RANGE_4G)
#define SENSOR_UX500_ACCELEROMETER_MAX (4.0f)
#define SENSOR_UX500_ACCELEROMETER_STEP \
	(SENSOR_UX500_ACCELEROMETER_MAX / 4096.0f)
#else
#error Unknown range
#endif

/* Gyroscopre defines */
#define L3G4200D_MODE_ON "1"
#define L3G4200D_MODE_OFF "0"
#define L3G4200D_RATE_100 "0"
#define L3G4200D_RANGE_250 "0"

#define SIZE_OF_BUF 27
#define CONVERT_A  (GRAVITY_EARTH * (1.0f/1000.0f))


/* ambient light defines */
#define BH1780GLI_ENABLE "3"
#define BH1780GLI_DISABLE "0"

/* proximity defines */
#define SFH7741_ENABLE "1"
#define SFH7741_DISABLE "0"

/* magnetometer paths*/
char const *const PATH_MODE_MAG =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-001e/mode";
char const *const PATH_RANGE_MAG =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-001e/range";
char const *const PATH_RATE_MAG =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-001e/rate";
char const *const PATH_DATA_MAG =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-001e/data";

/* accelerometer paths*/
char const *const PATH_MODE_ACC =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0018/mode";
char const *const PATH_RANGE_ACC =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0018/range";
char const *const PATH_RATE_ACC =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0018/rate";
char const *const PATH_DATA_ACC =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0018/data";
char const *const PATH_ID_ACC =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0018/id";

/* gyroscope paths */
char const *const PATH_MODE_GYR =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0068/powermode";
char const *const PATH_RANGE_GYR =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0068/range";
char const *const PATH_RATE_GYR =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0068/datarate";
char const *const PATH_DATA_GYR =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0068/gyrodata";

/* ambient light paths */
char const *const PATH_POWER_LUX =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0029/power_state";
char const *const PATH_DATA_LUX =
		"/sys/devices/platform/nmk-i2c.2/i2c-2/2-0029/lux";

/* proximity paths*/
char const *const PATH_POWER_PROX =
		"/sys/devices/platform/sensors1p.0/proximity_activate";
char const *const PATH_DATA_PROX =
		"/sys/devices/platform/sensors1p.0/proximity";
char const *const PATH_INTR_PROX =
                  "/dev/input/event0";

/* Proximity sensor structure */
typedef struct {
    int prox_val;
    char prox_flag;
}Sensor_prox;

/* sensor API integration */

static const struct sensor_t sSensorList[] = {
	{"Rohm BH1780GLI Ambient Light sensor",
		"ST-Ericsson AB",
		1,
		HANDLE_LIGHT,
		SENSOR_TYPE_LIGHT,
		65535.0f,
		1.0f,
		0.120f,
		MINDELAY_LIGHT,
		{}
	},
	{"SFH7741 Proximity sensor",
		"ST-Ericsson AB",
		1,
		HANDLE_PROXIMITY,
		SENSOR_TYPE_PROXIMITY,
		50.0f,
		1.0f,
		0.45f,
		MINDELAY_PROXIMITY,
		{}
	},
	{"LSM303DLH 3-axis Magnetic field sensor",
		"ST-Ericsson AB",
		1,
		HANDLE_MAGNETIC_FIELD,
		SENSOR_TYPE_MAGNETIC_FIELD,
		SENSOR_UX500_MAGNETOMETER_MAX,
		SENSOR_UX500_MAGNETOMETER_STEP,
		0.83f,
		MINDELAY_MAGNETIC_FIELD,
		{}
	},
	{"LSM303DLH 3-axis Accelerometer sensor",
		"ST-Ericsson AB",
		1,
		HANDLE_ACCELEROMETER,
		SENSOR_TYPE_ACCELEROMETER,
		SENSOR_UX500_ACCELEROMETER_MAX,
		SENSOR_UX500_ACCELEROMETER_STEP,
		0.83f,
		MINDELAY_ACCELEROMETER,
		{}
	},
	{"LSM303DLH 3-axis Orientation sensor",
		"ST-Ericsson AB",
		1,
		HANDLE_ORIENTATION,
		SENSOR_TYPE_ORIENTATION,
		360.0f,
		1.0f,
		1.66f,
		MINDELAY_ORIENTATION,
		{}
	},
	{"L3G4200D 3-axis Gyroscope sensor",
		"ST-Ericsson AB",
		1,
		HANDLE_GYROSCOPE,
		SENSOR_TYPE_GYROSCOPE,
		34.9f,
		0.0174f,
		0.83f,
		MINDELAY_GYROSCOPE,
		{}
	},
};

static int acc_id;
